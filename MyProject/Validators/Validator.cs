﻿using System;
using System.Text.RegularExpressions;


namespace MyProject
{
    public class Validator :IShower
    {
        private const string NameVal = @"[А-яа-я]";
        private const string LastNameVal = @"[А-яа-я]";
        private const string PhoneVal = @"\+\d{3} \(\d{2}\) \d{3}-\d{2}-\d{2}$";
        private const string PassVal = @"[A-Z]{2}\d{7}$";

        public event IShower.ConsoleWrite Write;
        public event IShower.ConsoleWriteLine WriteLine;

        //Проверка имени пользователя.
        public string ValidateName(string name)
        {
            bool isvalid = true;
            do
            {
                if (name.Length < 2 || name.Length > 10 || !Regex.IsMatch(name, NameVal))
                {
                    Console.Clear();
                    GetModel.Log.SaveLog($"{DateTime.UtcNow} Ошибка валидации имени клиента!");
                    Write?.Invoke("Некорректно введено имя клиента: ");
                    name = Console.ReadLine().ToLower();
                }
                else { isvalid = false; }
            } while (isvalid);
            return name;
        }

        //Проверка фамилии пользователя.
        public string ValidateLastName(string lastname)
        {
            bool isvalid = true;
            do
            {
                if (lastname.Length < 2 || lastname.Length > 15 || !Regex.IsMatch(lastname, LastNameVal))
                {
                    Console.Clear();
                    GetModel.Log.SaveLog($"{DateTime.UtcNow} Ошибка валидации фамилии клиента!");
                    Write?.Invoke("Некорректно введена фамилия клиента: ");
                    lastname = Console.ReadLine().ToLower();
                }
                else { isvalid = false; }
            } while (isvalid);
            return lastname;
        }

        //Проверка даты рождения пользователя.
        public string ValidateBirthday(string birthday)
        {
            bool isvalid = true;
            do
            {
                if (DateTime.TryParse(birthday, out DateTime date))
                {
                    var age = DateTime.Now.Year - date.Year;
                    if (age < 18 || age > 100)
                    {
                        Console.Clear();
                        GetModel.Log.SaveLog($"{DateTime.UtcNow} Ошибка валидации даты рождения клиента!");
                        Write?.Invoke("Дата рождения введена не правильно (по умолчанию от 18 до 100 лет): ");
                        birthday = Console.ReadLine();
                    }
                    else { isvalid = false; }
                }
                else
                {
                    Console.Clear();
                    GetModel.Log.SaveLog($"{DateTime.UtcNow} Ошибка валидации даты рождения клиента!");
                    Write?.Invoke("Дата рождения имеет недопустимый формат ( дд.мм.гггг ) : ");
                    birthday = Console.ReadLine();
                  //  GetModel.Validator.ValidateBirthday(birthday);
                }
            } while (isvalid);
            return birthday;
        }

        //Проверка номера телефона пользователя.
        public string ValidatePhone(string phone)
        {
            bool isvalid = true;
            do
            {
                if (!Regex.IsMatch(phone, PhoneVal))
                {
                    Console.Clear();
                    Write?.Invoke("Некорректно введен номер телефона ( +xxx (xx) xxx-xx-xx ): ");
                    GetModel.Log.SaveLog($"{DateTime.UtcNow} Ошибка валидации номера телефона клиента!");
                    phone = Console.ReadLine();
                }
                else { isvalid = false; }
            } while (isvalid);
            return phone;
        }

        // Проверка номера паспорта.
        public string ValidatePassport(string passport)
        {
            bool isvalid = true;
            do
            {
                if (!Regex.IsMatch(passport, PassVal))
                {
                    Console.Clear();
                    GetModel.Log.SaveLog($"{DateTime.UtcNow} Ошибка валидации номера паспорта клиента!");
                    Write?.Invoke("Некорректно введен номер паспорта ( XX1234567 XX - латинские символы) : ");
                    passport = Console.ReadLine();
                }
                else { isvalid = false; }
            } while (isvalid);
            return passport;
        }

        //Проверка корректности суммы.
        public string ValidateSumma(string summa)
        {
            bool isvalid = true;
            do
            {
                if (Convert.ToInt32(summa) < 0 || Convert.ToInt32(summa) > 1000000)
                {
                    Console.Clear();
                    GetModel.Log.SaveLog($"{DateTime.UtcNow} Ошибка валидации суммы платежа клиента!");
                    Write?.Invoke("Некорректно введена сумма (0 - 1.000.000): ");
                    summa = Console.ReadLine();
                }
                else { isvalid = false; }
            } while (isvalid);
            return summa;
        }     
    }
}
 