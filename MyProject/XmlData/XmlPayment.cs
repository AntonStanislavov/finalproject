﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace MyProject
{
    public class XmlPayment : IxPayment,IShower
    {
        public XmlSerializer xPayment = new XmlSerializer(typeof(Payment[]));
        public Payment[] ArrayPayment { get; set; }

        private string path = "xPayments.xml";

        public event IShower.ConsoleWrite Write;
        public event IShower.ConsoleWriteLine WriteLine;

        // Запись данных платежей клиентов в xml-файл.
        public void xWrite(List<Payment> payments)
        {
            File.Delete(path);
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                xPayment.Serialize(fs, payments.ToArray());
                WriteLine?.Invoke("База данных платежей сохранена.\n");
            }
        }

        // Чтение данных платежей клиентов из xml-файла.
        public List<Payment> xRead()
        {
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                try 
                {
                    ArrayPayment = (Payment[])xPayment.Deserialize(fs);
                    Write?.Invoke("\nБаза данных платежей подключена ");
                    if (fs.Length < 300)
                    {
                        WriteLine?.Invoke(" - записи отсутствуют.");
                    }
                    return PaymentRepository.Payment = ArrayPayment.ToList();
                }
                catch
                {
                    WriteLine?.Invoke("База данных платежей пуста.\n");
                    return PaymentRepository.Payment;
                }
            }
        }
    }
}
