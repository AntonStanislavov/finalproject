﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace MyProject
{
    public class XmlPerson : IxMember,IShower
    {
        public XmlSerializer xMember = new XmlSerializer(typeof(Member[]));
        public Member[] ArrayPeople { get; set; }

        private string path = "xMembers.xml";

        public event IShower.ConsoleWrite Write;
        public event IShower.ConsoleWriteLine WriteLine;

        // Запись данных клиентов в xml-файл.
        public void xWrite(List<Member> members)
        {
            File.Delete(path);
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                xMember.Serialize(fs, members.ToArray());
            }
            WriteLine?.Invoke("\n\nБаза данных клиентов сохранена.");
        }

        // Чтение данных клиентов из xml-файла.
        public List<Member> xRead()
        {
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
              //  Console.WriteLine(fs.Length);
                
                try
                {
                    ArrayPeople = (Member[])xMember.Deserialize(fs);
                    Write?.Invoke("\nБаза данных клиентов подключена ");
                    if (fs.Length < 300)
                    {
                        WriteLine?.Invoke(" - записи отсутствуют.");
                    }
                    return PeopleRepository.People = ArrayPeople.ToList();
                    
                }
                catch 
                {
                    WriteLine?.Invoke("\nБаза данных клиентов пуста.");
                    return PeopleRepository.People;
                }
            }
        }
    }
}
