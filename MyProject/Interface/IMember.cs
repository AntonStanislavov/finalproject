﻿using System;


namespace MyProject
{
    public interface IMember<T,V,S>
    {
        public void Create(T memberDTO, V members);
        public void Read(V members);
        public void Update(V members);
        public void Delete(V members, S payments);
        public void Search(V members);
    }
}
