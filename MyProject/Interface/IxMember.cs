﻿using System;
using System.Collections.Generic;

namespace MyProject
{
    public interface IxMember
    {
        public void xWrite(List<Member> members);
        public List<Member> xRead();
    }
}
