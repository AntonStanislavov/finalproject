﻿using System;


namespace MyProject
{
    public interface IShower
    {
        public delegate void ConsoleWrite(string message);
        public delegate void ConsoleWriteLine(string message);

        public event IShower.ConsoleWrite Write;
        public event IShower.ConsoleWriteLine WriteLine;
    }
}
