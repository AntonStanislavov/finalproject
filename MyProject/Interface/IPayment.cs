﻿using System;


namespace MyProject
{
    interface IPayment<T,V,S>
    {
        public void Create(T paymentDTO, S members);
        public void Read(V payments);
        public void Search(V payments, S members);
    }
}
