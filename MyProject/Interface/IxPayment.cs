﻿using System;
using System.Collections.Generic;

namespace MyProject
{
    public interface IxPayment
    {
        public void xWrite(List<Payment> payments);
        public List<Payment> xRead();
    }
}
