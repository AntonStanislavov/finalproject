﻿using System;


namespace MyProject
{
    [Serializable]
    public class Member : UniqueId
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string Phone { get; set; }
        public string Passport { get; set; }

    }
}
