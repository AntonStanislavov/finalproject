﻿using System;


namespace MyProject
{
    public class PaymentDTO
    {
        private string passport;
        private string summa;
        private string currency;

        public string Currency
        {
            get { return currency; }
            set
            {
                switch (value)
                {
                    case "1": { currency = "USD"; break; }
                    case "2": { currency = "EUR"; break; }
                    case "3": { currency = "RUS"; break; }
                    default:  { currency = "BYN"; break; }
                }
            }
        }
        public string Passport
        {
            get { return passport; }
            set { passport = GetModel.Validator.ValidatePassport(value); }
        }
        public string Summa 
        {
            get { return summa; }
            set { summa = GetModel.Validator.ValidateSumma(value); }
        }
    }
}
