﻿using System;


namespace MyProject
{
    [Serializable]
    public class Payment
    {
        public string Passport { get; set; }
        public string Summa { get; set; }
        public DateTime PaymentDate { get; set; }
        public string Currency { get; set; }
    }
}
