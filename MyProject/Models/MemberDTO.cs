﻿using System;


namespace MyProject
{
    public class MemberDTO : UniqueId
    {
        private string name;
        private string lastname; 
        private string birthday; 
        private string phone; 

        public string Name
        {
            get { return name;}
            set { name = GetModel.Validator.ValidateName(value); }
        }
        public string LastName
        {
            get { return lastname; }
            set { lastname = GetModel.Validator.ValidateLastName(value); }
        }
        public string Birthday
        {
            get { return birthday; }
            set { birthday = GetModel.Validator.ValidateBirthday(value); }
        }
        public string Phone
        {
            get { return phone; }
            set { phone = GetModel.Validator.ValidatePhone(value); }
        }
    }
}
