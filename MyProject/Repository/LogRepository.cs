﻿using System;
using System.Collections.Generic;


namespace MyProject
{
    public class LogRepository
    {
        public event IShower.ConsoleWriteLine WriteLine;
        public static List<string> Log  { get; private set; }
        public LogRepository()
        {
            Log = new List<string>();
        }
        public void GetLog()
        {
            if(Log.Count != 0)
            {
                Console.Clear();
                foreach (var line in Log)
                {
                    WriteLine?.Invoke(line);
                }
            }
            else
            {
                Console.Clear();
                WriteLine?.Invoke("Список Log-записей пуст.");
            }
        }
        public void SaveLog(string line)
        {
            Log.Add(line);
        }
    }
}
