﻿using System;


namespace MyProject
{
    public class Program 
    {
        static readonly GetModel model = new GetModel();
        static void Main(string[] args)
        {
            try
            {
                GetModel.XmlPerson.xRead();
                GetModel.XmlPayment.xRead();
                var members = PeopleRepository.People;
                for(int i = 0; i <members.Count; i++)
                {
                    members[i].Id = members[i].Name.GetHashCode() + members[i].LastName.GetHashCode();
                }

                model.StartProgram();
                GetModel.XmlPerson.xWrite(PeopleRepository.People);
                GetModel.XmlPayment.xWrite(PaymentRepository.Payment);
            }
            catch (Exception ex)
            {

                Console.WriteLine($"Что-то пошло не так... {ex.Message}");
            }
        }
    }
}
