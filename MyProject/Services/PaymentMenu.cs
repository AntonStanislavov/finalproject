﻿using System;


namespace MyProject
{
    public class PaymentMenu : IShower
    {
        public event IShower.ConsoleWriteLine WriteLine;
        public event IShower.ConsoleWrite Write;

        public void Getmenu()
        {
            bool back = true;
            do
            {
                WriteLine?.Invoke("\n\n\t\t\t\tВыберите пункт для продолжения работы:\n");
                WriteLine?.Invoke(" 1. Добавить новый платеж. \t\t\t 2. Просмотр списка всех платежей. \n\n 3. Поиск платежа по фамилии клиента. \t\t 4. Вернуться в главное меню.");
                var choicepayment = Console.ReadLine();
                switch (choicepayment)
                {
                    case "1":
                        {
                            if (PeopleRepository.People.Count != 0)
                            {
                                GetModel.PaymentHandler.Create(GetModel.Reader.ReadPayment(), PeopleRepository.People);
                            }
                            else
                            {
                                Console.Clear();
                                WriteLine?.Invoke("\nСписок клиентов пуст. Для работы с платежами внесите клиента в базу.\n");
                                GetModel.NewClient.NewClient();
                            } 
                            break;
                        }
                    case "2":
                        {
                            if (PeopleRepository.People.Count != 0)
                            {
                                GetModel.PaymentHandler.Read(PaymentRepository.Payment);
                            }
                            else
                            {
                                Console.Clear();
                                WriteLine?.Invoke("\nСписок клиентов пуст. Для работы с платежами внесите клиента в базу.\n");
                                GetModel.NewClient.NewClient();
                            }
                            break;
                        }
                    case "3":
                        {
                            if (PeopleRepository.People.Count != 0)
                            {
                                GetModel.PaymentHandler.Search(PaymentRepository.Payment, PeopleRepository.People); 
                            }
                            else
                            {
                                Console.Clear();
                                WriteLine?.Invoke("\nСписок клиентов пуст. Для работы с платежами внесите клиента в базу.\n");
                                GetModel.NewClient.NewClient();
                            }
                            break;
                        }
                    case "4": { Console.Clear(); back = false;  break; }
                    default: Console.Clear(); break;
                }
            } while (back);
        }
    }
}
