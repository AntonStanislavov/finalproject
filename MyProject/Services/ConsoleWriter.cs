﻿using System;


namespace MyProject
{
    public class Writer 
    {
        public void ConsoleWriteLine(string message)
        {
            Console.WriteLine(message);
        }
        public void ConsoleWrite(string message)
        {
            Console.Write(message);
        }
    }
}
