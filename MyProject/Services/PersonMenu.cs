﻿using System;


namespace MyProject
{
    public class PersonMenu : IShower
    {
        public event IShower.ConsoleWriteLine WriteLine;
        public event IShower.ConsoleWrite Write;


        public void Getmenu() 
        {
            bool back = true;
            do
            {
                WriteLine?.Invoke("\n\n\t\t\t\tВыберите пункт для продолжения работы:\n");
                WriteLine?.Invoke(" 1. Добавление нового клиента.\t\t 2. Просмотр списка клиентов. \t  3. Поиск клиента по фамилии.\n " +
                    "\n 4. Редактирование данных клиента.       5. Удаление клиента из базы.     6. Вернуться в главное меню.");
                var choiceperson = Console.ReadLine();
                switch (choiceperson)
                {
                    case "1":
                        {
                            GetModel.PersonHandler.Create(GetModel.Reader.ReadMember(), PeopleRepository.People);
                            break;
                        }
                    case "2":
                        {
                            GetModel.PersonHandler.Read(PeopleRepository.People);
                            break;
                        }
                    case "3":
                        {
                            GetModel.PersonHandler.Search(PeopleRepository.People);
                            break;
                        }
                    case "4":
                        {
                            GetModel.PersonHandler.Update(PeopleRepository.People);
                            break;
                        }
                    case "5":
                        {
                            GetModel.PersonHandler.Delete(PeopleRepository.People, PaymentRepository.Payment);
                            break;
                        }
                    case "6":
                        {
                            Console.Clear();
                            back = false;
                            break;
                        }
                    default: Console.Clear(); break;
                }
            } while (back);
        }
    }
}
