﻿using System;


namespace MyProject
{
    public class ConsoleReader : IShower
    {
        public event IShower.ConsoleWrite Write;
        public event IShower.ConsoleWriteLine WriteLine;

        public MemberDTO ReadMember()
        {
            
            var memberdto = new MemberDTO();

            Console.Clear();

            Write?.Invoke("Введите Имя: ");
            memberdto.Name = Console.ReadLine().ToLower();

            Write?.Invoke("Введите Фамилию: ");
            memberdto.LastName = Console.ReadLine().ToLower();

            Write?.Invoke("Введите дату своего рождения в формате дд.мм.гггг: ");
            memberdto.Birthday = Console.ReadLine();

            Write?.Invoke("Введите ваш номер телефона в формате: +ххх (хх) ххх-хх-хх: ");
            memberdto.Phone = Console.ReadLine();

            return memberdto;
        } 
        public PaymentDTO ReadPayment()
        {
            var paymentdto = new PaymentDTO();

            Console.Clear();

            Write?.Invoke("\nВведите серию и номер паспорта в формате ХХ1234567: ");
            paymentdto.Passport = Console.ReadLine();

            Write?.Invoke("\nВыберите валюту платежа (BYN по умолчанию):\n\n" + "\t1 - USD    2 - EUR   3 - RUS:  ");
            paymentdto.Currency = Console.ReadLine();

            Write?.Invoke("\nВведите сумму платежа: ");
            paymentdto.Summa = Console.ReadLine();


            return paymentdto;
        }
    }
}
