﻿using System;
using System.Collections.Generic;

namespace MyProject
{
    public class PaymentHandler: IShower
    {
        public event IShower.ConsoleWrite Write;
        public event IShower.ConsoleWriteLine WriteLine;

        // Создание нового платежа.
        public void Create(PaymentDTO paymentDTO, List<Member> members)
        {
            Console.Clear();
            var found = GetModel.Search.Search(members);
            Write?.Invoke("\n\nВыберите клиента из списку по номеру: ");
            int number = Convert.ToInt32(Console.ReadLine());
            if (number < found.Count)
            {
                found[number].Passport = paymentDTO.Passport;
                var payment = new Payment()
                {
                    PaymentDate = DateTime.UtcNow,
                    Passport = paymentDTO.Passport,
                    Summa = paymentDTO.Summa,
                    Currency = paymentDTO.Currency,
                };
                PaymentRepository.Payment.Add(payment);
                Console.Clear();
                WriteLine?.Invoke($"\nПлатеж успешно создан:\n");
                WriteLine?.Invoke("\t Фамилия        Имя        Номер паспорта     Сумма платежа             Дата платежа");
                WriteLine?.Invoke("\t-----------------------------------------------------------------------------------------");
                Console.WriteLine("\n\t{0,-15}{1,-15}{2,-17}{3,-1} {4,-15}{5,-15}", found[number].LastName, found[number].Name, payment.Passport, payment.Summa, payment.Currency, payment.PaymentDate);
            }
            else { Console.Clear(); WriteLine?.Invoke("Номер клиента введен не верно."); }
        }

        // Список всех платежей 
        public void Read(List<Payment> payments)
        {
            Console.Clear();
            WriteLine?.Invoke("\t №    Номер паспорта        Сумма платежа             Дата платежа");
            WriteLine?.Invoke("\t----------------------------------------------------------------------");
            if (payments.Count != 0)
            {
                int i = 0;
                foreach (var pay in payments)
                {
                    Console.WriteLine("\n\t {0,-8}{1,-20}{2,-7} {3,-15}{4,-15}",i++, pay.Passport, pay.Summa, pay.Currency, pay.PaymentDate);
                }
            }
            else { Console.Clear(); WriteLine?.Invoke("Список платежей пуст."); }
        }
       
        // Поиск платежей по фамилии.
        public void Search(List<Payment> payments, List<Member> members)
        {
            if (payments.Count != 0)
            {
                var found = GetModel.Search.Search(members);
                if(found.Count != 0)
                {
                    Write?.Invoke("\n\nВыберите клиента из списку по номеру: ");
                    int number = Convert.ToInt32(Console.ReadLine());
                    if (number < found.Count)
                    {
                        Console.Clear();
                        Write?.Invoke("Введите номер паспорта клиента: ");
                        var passport = GetModel.Validator.ValidatePassport(Console.ReadLine());
                        List<Payment> foundpay = payments.FindAll(pay => passport.GetHashCode() == found[number].Passport.GetHashCode());
                        if (foundpay.Count != 0)
                        {
                            Console.Clear();
                            WriteLine?.Invoke($"\n\tСписок платежей для клиента: {found[number].LastName.ToUpper()}  {found[number].Name.ToUpper()}");
                            WriteLine?.Invoke("\n\n\t №    Номер паспорта        Сумма платежа             Дата платежа");
                            WriteLine?.Invoke("\t----------------------------------------------------------------------");
                            int j = 1;
                            foreach (Payment pay in foundpay)
                            {
                                Console.WriteLine("\n\t {0,-8}{1,-20}{2} {3,-15}{4,-15}", j++, pay.Passport, pay.Summa, pay.Currency, pay.PaymentDate);
                            }
                        }
                        else { Console.Clear(); WriteLine?.Invoke("Данные о платежах отсутствуют."); }
                    }
                    else { Console.Clear(); WriteLine?.Invoke("Не верно выбран номер клиента."); }
                } 
            }
            else { Console.Clear(); WriteLine?.Invoke("Cписок платежей пуст."); }
        }   
    }
}
