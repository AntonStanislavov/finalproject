﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;


namespace MyProject
{
    public class PersonHandler : IShower
    {
        public event IShower.ConsoleWriteLine WriteLine;
        public event IShower.ConsoleWrite Write;

        public XmlSerializer xMember = new XmlSerializer(typeof(Member[]));

        // Добавление нового клиента.
        public void Create(MemberDTO memberDTO, List<Member> members)
        {
            Console.Clear();
            memberDTO.Id = memberDTO.Name.GetHashCode() + memberDTO.LastName.GetHashCode() + DateTime.Parse(memberDTO.Birthday).ToString().GetHashCode();
            var member = new Member()
            {
                Name = memberDTO.Name,
                LastName = memberDTO.LastName,
                Birthday = DateTime.Parse(memberDTO.Birthday),
                Phone = memberDTO.Phone,
                Id = memberDTO.LastName.GetHashCode() + memberDTO.Name.GetHashCode(),
                Passport = "No payments"
                
            };
            var control = members.Exists(member => member.Id + member.Birthday.ToString().GetHashCode() == memberDTO.Id);
            if (control != true)
            {
                PeopleRepository.People.Add(member);
                Console.Clear();
                WriteLine?.Invoke($"Новый клиент: {member.Name.ToUpper()} {member.LastName.ToUpper()} добавлен в список.");
                GetModel.NewClient.NewClient();
            }
            else { Console.Clear(); WriteLine?.Invoke("Клиент с такими данными уже есть в базе данных.\n\n"); GetModel.NewClient.NewClient(); }
        }

        // Получение списка всех пользователей.
        public void Read(List<Member> members)
        {
            Console.Clear();
            WriteLine?.Invoke("\t№    Фамилия       Имя         Дата рождения        № телефона");
            WriteLine?.Invoke("\t-------------------------------------------------------------------");
            if (members.Count != 0 & members != null)
            {
                int i = 0;
                foreach (var p in members)
                {
                    Console.WriteLine("\t{0,-3}{1,-15}{2,-15}{3,-15}{4,-15}", i++, p.LastName, p.Name, p.Birthday.ToShortDateString(), p.Phone);
                }
            }
            else { Console.Clear(); WriteLine?.Invoke("Список клиентов пуст."); }
        }

        // Редактирование данных пользователя.
        public void Update(List<Member> members)
        {
            Console.Clear();
            if (members.Count != 0)
            {
                WriteLine?.Invoke("\t\nВыберите клиента для редактирования данных:\n\n ");
                WriteLine?.Invoke("\t№    Фамилия       Имя         Дата рождения        № телефона");
                WriteLine?.Invoke("\t-------------------------------------------------------------------");
                int i = 0;
                foreach (var old in members)
                {
                    Console.WriteLine("\t{0,-3}{1,-15}{2,-15}{3,-15}{4,-15}", i++, old.LastName, old.Name, old.Birthday.ToShortDateString(), old.Phone);
                }
                Write?.Invoke("\n\nВыберите клиента из списку по номеру: ");
                int number = Convert.ToInt32(Console.ReadLine());
                if (number < members.Count)
                {
                    bool stop = true;
                    do
                    {
                        Console.Clear();
                        WriteLine?.Invoke("\t№    Фамилия       Имя         Дата рождения        № телефона");
                        WriteLine?.Invoke("\t-------------------------------------------------------------------");
                        Console.WriteLine("\t{0,-3}{1,-15}{2,-15}{3,-15}{4,-15}\n\n\n", number,members[number].LastName,members[number].Name, members[number].Birthday.ToShortDateString(), members[number].Phone);
                        WriteLine?.Invoke("\t\t\tВыберите данные для исправления: \n\n 1. Изменить имя клиента. \t\t 2. Изменить фамилию клиента." +
                        " \t\t 3. Изменить дату рождения клиента. \n\n 4. Изменить номер телефона клиента. \t 5. Завершить исправления данных.");
                        var a = Console.ReadLine();
                        switch (a)
                        {
                            case "1":
                                {
                                    Console.Clear();
                                    Write?.Invoke($"Имя: {members[number].Name}. Введите исправления: ");
                                    members[number].Name = GetModel.Validator.ValidateName(Console.ReadLine().ToLower());
                                    members[number].Id = members[number].LastName.GetHashCode() + members[number].Name.GetHashCode();
                                    break;
                                }
                            case "2":
                                {
                                    Console.Clear();
                                    Write?.Invoke($"Фамилия: {members[number].LastName}. Введите исправления: ");
                                    members[number].LastName = GetModel.Validator.ValidateLastName(Console.ReadLine().ToLower());
                                    members[number].Id = members[number].LastName.GetHashCode() + members[number].Name.GetHashCode();
                                    break;
                                }
                            case "3":
                                {
                                    Console.Clear();
                                    Write?.Invoke($"Дата рождения: {members[number].Birthday.ToShortDateString()}. Введите исправления: ");
                                    members[number].Birthday = Convert.ToDateTime(GetModel.Validator.ValidateBirthday(Console.ReadLine()));                                    
                                    break;
                                }
                            case "4":
                                {
                                    Console.Clear();
                                    Write?.Invoke($"Номер телефона: {members[number].Phone}. Введите исправления: ");
                                    members[number].Phone = GetModel.Validator.ValidatePhone(Console.ReadLine());
                                    break;
                                }
                            case "5": { stop = false; Console.Clear(); break; }
                        }
                    } while (stop);
                }
                else { Console.Clear(); WriteLine?.Invoke("Номер клиента введен не верно."); }
            }
            else { Console.Clear(); WriteLine?.Invoke("Cписок клиентов пуст."); }
        }

        // Удаление пользователя.
        public void Delete(List<Member> members, List<Payment> payments)
        {
            Console.Clear();
            if (members.Count != 0)
            {
                var found = GetModel.Search.Search(members);
                if (found.Count != 0)
                {
                    Write?.Invoke("\n\nВыберите клиента из списку по номеру: ");
                    int number = Convert.ToInt32(Console.ReadLine());
                    if (number < found.Count)
                    {
                        var delete = members.RemoveAll(item => found[number].LastName.GetHashCode() + found[number].Name.GetHashCode() == item.Id);
                        var deletpay = payments.RemoveAll(item => found[number].Passport.GetHashCode() == item.Passport.GetHashCode());

                        if (delete != 0)
                        {
                            Console.Clear();
                            WriteLine?.Invoke($"Клиент {found[number].LastName.ToUpper()} {found[number].Name.ToUpper()} и его платежи {deletpay} шт. удалены из базы.");
                        }
                        else { Console.Clear(); WriteLine?.Invoke("Клиент с такими именем и фамилией не найден."); }
                    }
                    else { Console.Clear(); WriteLine?.Invoke("Не верно выбран номер клиента."); }
                }
            }
            else { Console.Clear(); WriteLine?.Invoke("Cписок клиентов пуст."); }
        }

        // Поиск клиента в базе данных.
        public void Search(List<Member> members)
        {
            Console.Clear();
            if (members.Count != 0)
            {
                bool yes = true;
                do
                {
                    Console.Clear();
                    Write?.Invoke("\n\nВведите фамилию клиента: ");
                    var surname = Console.ReadLine().ToLower();
                    var found = members.FindAll(item => item.LastName.StartsWith(surname));
                    if (found.Count == 0)
                    {
                        Console.Clear();
                        WriteLine?.Invoke("\nКлиент с такой фамилией отсутствует в базе.\n" + "\nПродолжить поиск?\n" + "\n1 - Да   2 - Нет\n");
                        string search = Console.ReadLine();
                        switch (search)
                        {
                            case "1": { break; }
                            default: { Console.Clear(); yes = false; break; }
                        }
                    }
                    else
                    {
                        Console.Clear();
                        WriteLine?.Invoke("\t№    Фамилия       Имя         Дата рождения        № телефона");
                        WriteLine?.Invoke("\t-------------------------------------------------------------------");
                        int i = 0;
                        foreach (Member member in found)
                        {
                            Console.WriteLine("\t{0,-3}{1,-15}{2,-15}{3,-15}{4,-15}", i++, member.LastName, member.Name, member.Birthday.ToShortDateString(), member.Phone);
                        }
                        yes = false;
                    }   
                } while (yes);
            }
            else { Console.Clear(); WriteLine?.Invoke("Cписок клиентов пуст."); }
        }          
    } 
}
   