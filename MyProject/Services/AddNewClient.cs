﻿using System;


namespace MyProject
{
    public class AddNewClient :IShower
    {
        public event IShower.ConsoleWriteLine WriteLine;
        public event IShower.ConsoleWrite Write;

        public void NewClient()
        {
            WriteLine?.Invoke("\nДобавить нового клиента?\n\n" + "\t 1 - Да \t 2 - Нет\n");
            var add = Console.ReadLine();
            switch (add)
            {
                case "1": { Console.Clear(); GetModel.PersonHandler.Create(GetModel.Reader.ReadMember(), PeopleRepository.People); break; }
                default: { Console.Clear(); break; }
            }
        }
    } 
}

