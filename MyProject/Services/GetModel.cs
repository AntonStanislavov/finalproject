﻿using System;
using System.Collections.Generic;

namespace MyProject
{
    class GetModel 
    {
        public static AddNewClient NewClient { get; private set; }
        public static ConsoleReader Reader { get; private set; }
        public static Writer Writer { get; private set; }
        public static PaymentHandler PaymentHandler { get; private set; }
        public static PaymentMenu PaymentMenu { get; private set; }
        public static PersonHandler PersonHandler { get; private set; }
        public static PersonMenu PersonMenu { get; private set; }
        public static StartMenu StartMenu { get; private set; }
        public static Validator Validator { get; private set; }
        public static LogRepository Log { get; private set; }
        public static SearchMember Search { get; private set; }
        public static XmlPerson XmlPerson { get; private set; }
        public static XmlPayment XmlPayment { get; private set; }


        static GetModel()
        {
            NewClient = new AddNewClient();
            Reader = new ConsoleReader();
            Writer = new Writer();
            PaymentHandler = new PaymentHandler();
            PersonMenu = new PersonMenu();
            PaymentMenu = new PaymentMenu();
            PersonHandler = new PersonHandler();
            StartMenu = new StartMenu();
            Validator = new Validator();
            Log = new LogRepository();
            Search = new SearchMember();
            XmlPerson = new XmlPerson();
            XmlPayment = new XmlPayment();

            NewClient.WriteLine += Writer.ConsoleWriteLine;
            Reader.Write += Writer.ConsoleWrite;
            PaymentHandler.Write += Writer.ConsoleWrite;
            PaymentHandler.WriteLine += Writer.ConsoleWriteLine;
            PaymentMenu.WriteLine += Writer.ConsoleWriteLine;
            PersonHandler.Write += Writer.ConsoleWrite;
            PersonHandler.WriteLine += Writer.ConsoleWriteLine;
            PersonMenu.WriteLine += Writer.ConsoleWriteLine;
            StartMenu.WriteLine += Writer.ConsoleWriteLine;
            Validator.Write += Writer.ConsoleWrite;
            Log.WriteLine += Writer.ConsoleWriteLine;
            Search.Write += Writer.ConsoleWrite;
            Search.WriteLine += Writer.ConsoleWriteLine;
            XmlPerson.WriteLine += Writer.ConsoleWriteLine;
            XmlPerson.Write += Writer.ConsoleWrite;
            XmlPayment.WriteLine += Writer.ConsoleWriteLine;
            XmlPayment.Write += Writer.ConsoleWrite;
        }

        public void StartProgram()
        {
            StartMenu.Start();
        }
    }
}

