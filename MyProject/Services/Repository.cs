﻿using System;
using System.Collections.Generic;


namespace MyProject
{
    public abstract class Repository<T,V>
    {
        public abstract void Create(T obj);
        public abstract void Read(V obj);
        public abstract void Update(V obj);
        public abstract void Delete(V obj);
        public abstract void Search(V obj);
    }
}
