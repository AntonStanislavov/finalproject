﻿using System;


namespace MyProject
{
    public class StartMenu : IShower

    {
        public event IShower.ConsoleWriteLine WriteLine;
        public event IShower.ConsoleWrite Write;

        public void Start()
        {
            bool exit = true;
            do
            {
                WriteLine?.Invoke("\n\n\t\t\tЗдравствуйте!\n\n\tВыберите пункт меню для продолжения работы приложения\n\n   1. Работа с клиентами.\t" +
                    " \t\t2. Работа с платежами.\t\t\n\n   3. Просмотр записей в Log-файле \t\t4. Выход из программы.");
                var choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        {
                            Console.Clear();
                            GetModel.PersonMenu.Getmenu();
                            break;
                        }
                    case "2":
                        {
                            Console.Clear();
                            GetModel.PaymentMenu.Getmenu();
                            break;
                        }
                    case "3":
                        {
                            Console.Clear();
                            GetModel.Log.GetLog();
                            break;
                        }
                    case "4":
                        {
                            exit = false;
                            break;
                        }
                    default: Console.Clear(); break;
                }
            } while (exit);
        }
    }
}
