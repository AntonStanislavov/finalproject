﻿using System;
using System.Collections.Generic;

namespace MyProject
{
    public class SearchMember : IShower
    {
        public event IShower.ConsoleWrite Write;
        public event IShower.ConsoleWriteLine WriteLine;

        public List<Member> Found { get; set; }

        public List<Member> Search(List<Member> members)
        {
            bool yes = true;
            do
            {
                Console.Clear();
                Write?.Invoke("\n\nВведите фамилию клиента: ");
                var surname = Console.ReadLine().ToLower();
                Found = members.FindAll(item => item.LastName.StartsWith(surname));
                if (Found.Count == 0)
                {
                    Console.Clear();
                    WriteLine?.Invoke("\nКлиент с такой фамилией отсутствует в базе.\n" + "\nПродолжить поиск?\n" + "\n1 - Да   2 - Нет\n");
                    string search = Console.ReadLine();
                    switch (search)
                    {
                        case "1": { break; }
                        default: { Console.Clear(); yes = false; break; }
                    }
                }
                else
                {
                    Console.Clear();
                    WriteLine?.Invoke("\t№    Фамилия       Имя         Дата рождения        № телефона");
                    WriteLine?.Invoke("\t-------------------------------------------------------------------");
                    int i = 0;
                    foreach (Member member in Found)
                    {
                        Console.WriteLine("\t{0,-3}{1,-15}{2,-15}{3,-15}{4,-15}", i++, member.LastName, member.Name, member.Birthday.ToShortDateString(), member.Phone);
                    }
                    yes = false;
                }
            } while (yes);
            return Found;
        }
    }
}
